import sys

from client.resource_manager_client import ResourceManagerClient

if __name__ == "__main__":
    query = sys.argv[1]
    client = ResourceManagerClient()
    client.connect()
    print(client.send_command({"command": "acquire", "query": query}))
    client.close()