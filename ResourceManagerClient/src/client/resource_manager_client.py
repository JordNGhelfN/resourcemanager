from multiprocessing.connection import Client


class ResourceManagerClient(object):

    def __init__(self) -> None:
        self.client = None

    def connect(self):
        address = ('localhost', 6000)
        self.client = Client(address)

    def send_command(self, command):
        self.client.send(command)
        return self.client.recv()
    
    def close(self):
        self.client.close()