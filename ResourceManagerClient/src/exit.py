import sys

from client.resource_manager_client import ResourceManagerClient

if __name__ == "__main__":
    client = ResourceManagerClient()
    client.connect()
    print(client.send_command({"command": "exit"}))
    client.close()