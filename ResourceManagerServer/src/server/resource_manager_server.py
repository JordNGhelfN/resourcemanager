from multiprocessing.connection import Listener
from manager.queries.resource_query_parser import ResourceQueryParser
from manager.exceptions.command_execution_exception import CommandExecutionException
from manager.resource_manager import ResourceManager
from manager.test_resource import TestResource
import json


class ResourceManagerServer(object):

    def __init__(self) -> None:
        self.listener = None
        self.manager = ResourceManager(TestResource)
        self.query_parser = ResourceQueryParser()

    def start(self):
        self.manager.start()
        address = ('localhost', 6000)
        self.listener = Listener(address)
        self.listen_loop()

    def listen_loop(self):
        while True:
            conn = self.listener.accept()
            msg = conn.recv()
            print("Got command:", msg)
            if msg["command"] == "acquire":
                try:
                    query = self.query_parser.parse_query(msg["query"])
                    identifier = self.manager.find_and_acquire_resource(query)
                    print("acquired resource", identifier)
                    conn.send({"result": "success", "identifier": identifier})
                except CommandExecutionException as e:
                    print("Error executing command", str(type(e)))
                    conn.send({"result": "error", "error": str(type(e))})
            if msg["command"] == "release":
                self.manager.release_resource(msg["identifier"])
            if msg["command"] == "exit":
                exit(0)


