from manager.owner_death_monitor import OwnerDeathMonitor
from manager.resource import Resource
from typing import Type
from manager.exceptions.all_matching_resources_busy_exception import AllMatchingResourcesBustException
from manager.exceptions.no_such_resource_exception import NoSuchResourceException
from manager.queries.resource_query import ResourceQuery
from manager.resource_entry import ResourceEntry
from manager.resource_finder import ResourceFinder


class ResourceManager(object):
    def __init__(self, resource_type: Type[Resource]) -> None:
        self.resources = [ResourceEntry(resource) for resource in resource_type.get_available_resources()]
        self.resource_finder = ResourceFinder()
        self.death_monitor = OwnerDeathMonitor(self.resources)

    def start(self):
        self.death_monitor.start_monitoring()

    def acquire_resource(self, resource_entry: ResourceEntry, calling_pid: int):
        resource_entry.owner_pid = calling_pid
        resource_entry.resource.acquire()

    def find_and_acquire_resource(self, query: ResourceQuery):
        matching_resources = self.resource_finder.find_matching_resources(self.resources, query)
        if len(matching_resources) == 0:
            raise NoSuchResourceException()
        for resource in matching_resources:
            if resource.owner_pid == None:
                self.acquire_resource(resource)
                return resource.resource.identifier
        raise AllMatchingResourcesBustException()

    def release_resource(self, identifier):
        for resource in self.resources:
            if resource.resource.identifier == identifier:
                resource.owner_pid = None