from manager.queries.resource_query import ResourceQuery
from manager.resource_entry import ResourceEntry
from typing import List


class ResourceFinder(object):

    def find_matching_resources(self, resources_list: List[ResourceEntry], query: ResourceQuery) -> List[ResourceEntry]:
        matched_resources = []
        for resource in resources_list:
            is_match = True
            for query_part in query.parts:
                current_resource_value = resource.resource.parameters[query_part.parameter_name]
                if not query_part.does_match_value(current_resource_value):
                    is_match = False
            if is_match:
                matched_resources.append(resource)
        return matched_resources
