from abc import ABC, abstractmethod, abstractclassmethod
from typing import List, Dict


class Resource(ABC):

    def __init__(self, identifier: str, parameters: Dict[str, object]) -> None:
        self.identifier = identifier
        self.parameters = parameters

    @abstractmethod
    def acquire(self):
        pass

    @abstractmethod
    def release(self):
        pass

    @abstractclassmethod
    def get_available_resources(cls):
        pass
