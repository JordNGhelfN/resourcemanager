from typing import Dict
from manager.resource import Resource

class TestResource(Resource):
    def __init__(self, identifier: str, parameters: Dict[str, object]) -> None:
        super().__init__(identifier, parameters)

    def acquire(self):
        print("acquired identifier", self.identifier)

    def release(self):
        print("released identifier", self.identifier)

    @classmethod
    def get_available_resources(cls):
        return [
            TestResource("r1", {"param_s": "value1", "param_i": 1}),
            TestResource("r2", {"param_s": "value2", "param_i": 2}),
        ]