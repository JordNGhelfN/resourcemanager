
from manager.exceptions.command_execution_exception import CommandExecutionException


class NoSuchResourceException(CommandExecutionException):
    pass
