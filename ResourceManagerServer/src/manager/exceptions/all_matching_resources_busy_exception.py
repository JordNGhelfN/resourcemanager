
from manager.exceptions.command_execution_exception import CommandExecutionException


class AllMatchingResourcesBustException(CommandExecutionException):
    pass