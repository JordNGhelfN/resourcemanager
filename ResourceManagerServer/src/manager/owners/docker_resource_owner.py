

from manager.owners.resource_owner import ResourceOwner


class DockerResourceOwner(ResourceOwner):

    def __init__(self, container_id) -> None:
        self.container_id = container_id

    def check_if_dead(self):
        return False # TODO parse docker ps to check if the container name is there.
