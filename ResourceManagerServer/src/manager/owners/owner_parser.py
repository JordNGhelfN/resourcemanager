

from manager.owners.docker_resource_owner import DockerResourceOwner


class OwnerParser(object):

    def parse(self, owner_json):
        if owner_json["type"] == "docker":
            return DockerResourceOwner(owner_json["container_id"])