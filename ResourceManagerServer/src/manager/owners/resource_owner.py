from abc import ABC, abstractmethod

class ResourceOwner(ABC):

    @abstractmethod
    def check_if_dead(self):
        pass