from manager.owners.resource_owner import ResourceOwner
from manager.resource import Resource


class ResourceEntry(object):
    def __init__(self, resource):
        self.resource: Resource = resource
        self.owner: ResourceOwner = None
