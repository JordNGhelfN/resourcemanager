from typing import Any
from manager.queries.query_comparison_type import QueryComparisonType


class ResourceQueryPart(object):

    def __init__(self, parameter_name, comparison_type, value) -> None:
        self.parameter_name: str = parameter_name
        self.comparison_type: QueryComparisonType = comparison_type
        self.value: Any = value

    def does_match_value(self, value) -> bool:
        if self.comparison_type == QueryComparisonType.LESS_THAN:
            return value < self.value
        if self.comparison_type == QueryComparisonType.GREATER_THAN:
            return value > self.value
        if self.comparison_type == QueryComparisonType.LESS_EQUALS:
            return value <= self.value
        if self.comparison_type == QueryComparisonType.GREATER_EQUALS:
            return value >= self.value
        if self.comparison_type == QueryComparisonType.EQUALS:
            return value == self.value