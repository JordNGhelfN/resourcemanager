

from typing import List

from manager.queries.resource_query_part import ResourceQueryPart


class ResourceQuery(object):

    def __init__(self, parts) -> None:
        self.parts: List[ResourceQueryPart] = parts
