from enum import Enum


QueryComparisonType = Enum("QueryComparisonType", ["LESS_THAN", "GREATER_THAN", "LESS_EQUALS", "GREATER_EQUALS", "EQUALS"])