import re
from manager.queries.query_comparison_type import QueryComparisonType

from manager.exceptions.invalid_query_exception import InvalidQueryException
from manager.queries.resource_query import ResourceQuery
from manager.queries.resource_query_part import ResourceQueryPart

QUERY_PART_REGEX = r"(?P<parameter>\w+)(?P<comparison_type>>|<|<=|>=|==)(?P<value>\w+)$"
INT_VALUE_REGEX = r""

COMPARISON_TYPE_STRING_TO_ENUM = {
    '>': QueryComparisonType.GREATER_THAN,
    '<': QueryComparisonType.LESS_THAN,
    '==': QueryComparisonType.EQUALS,
    '<=': QueryComparisonType.LESS_EQUALS,
    '>=': QueryComparisonType.GREATER_EQUALS,
}

class ResourceQueryParser(object):

    def parse_query(self, query_string: str):
        query_parts = []
        query_parts_strings = query_string.split(";")
        for query_part_string in query_parts_strings:
            query_parts.append(self.parse_query_part(query_part_string))

        return ResourceQuery(query_parts)


    def parse_query_part(self, query_part_string: str):
        regex_match = re.match(QUERY_PART_REGEX, query_part_string)
            
        if regex_match is None:
            raise InvalidQueryException()

        parameter = regex_match.group('parameter')
        comparison_type = COMPARISON_TYPE_STRING_TO_ENUM[regex_match.group('comparison_type')]
        value = self.parse_parameter_value(regex_match.group('value'))
        return ResourceQueryPart(parameter, comparison_type, value)

    def parse_parameter_value(self, value_string: str):
        try:
            int_value = int(value_string)
            return int_value
        except ValueError:
            pass

        try:
            float_value = float(value_string)
            return float_value
        except ValueError:
            pass

        return value_string
