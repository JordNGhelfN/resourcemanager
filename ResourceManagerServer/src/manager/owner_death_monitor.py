

from threading import Thread
import time
from typing import List
from manager.resource_entry import ResourceEntry


SECONDS_BETWEEN_POLLS = 3


class OwnerDeathMonitor(object):
    
    def __init__(self, entries: List[ResourceEntry]) -> None:
        self.entries = entries

    def start_monitoring(self):
        Thread(target=self.monitor_deaths).start()

    def monitor_deaths(self):
        while True:
            for entry in self.entries:
                if entry.owner.check_if_dead():
                    entry.owner = None

            time.sleep(SECONDS_BETWEEN_POLLS)
